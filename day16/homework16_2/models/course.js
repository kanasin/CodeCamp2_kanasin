module.exports = {
    async courseByPrice(pool, price){
        const [rows] = await pool.query(`select * from courses where price = ?`, [price])
        return rows
    },
    async courseById(pool, id){
        const [rows] = await pool.query(`select * from courses where id = ?`, [id])
        return rows
    }
}