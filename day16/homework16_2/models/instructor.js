module.exports = {
    async instructorsAll(pool){
        const [rows] = await pool.query(`select * from instructors`)
        return rows
    },
    async instructorById(pool, id){
        const [rows] = await pool.query(`select * from instructors where id = ?`, [id])
        return rows
    }
}