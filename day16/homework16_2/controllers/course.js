module.exports = function (model, pool) {
    return {
        async coursesByPrice(ctx, next) {
            const courses = await model.courseByPrice(pool, ctx.params.price)
            await ctx.render('table', {data: courses})
            await next()
        },
        async coursesById(ctx, next){
            const course = await model.courseById(pool, ctx.params.id)
            await ctx.render('table', {data: course})
            await next()
        }
    }
}