const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')
const render = require('koa-ejs')
const path = require('path')
const instructorManage = require('./controllers/instructor')
const instructorModel = require('./models/instructor')
const courseManage = require('./controllers/course')
const courseModel = require('./models/course')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'codecamp'
})

const instructorController = instructorManage(instructorModel, pool)
const courseController = courseManage(courseModel, pool)

router.get('/instructor/find_all', instructorController.instructorsAll)
router.get('/instructor/find_by_id/:id', instructorController.instructorsById)
router.get('/course/find_by_id/:id', courseController.coursesById)
router.get('/course/find_by_price/:price', courseController.coursesByPrice)




app.use(router.routes())
app.use(router.allowedMethods())
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


app.listen(3000)