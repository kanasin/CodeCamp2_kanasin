module.exports = function (model, pool) {
    return {
        async coursesByPrice(ctx, next) {
            const courses = await model.courseByPrice(pool, ctx.params.price)
            //console.log(courses)
            await ctx.render('table', {data: courses})
            await next()
        },
        async coursesById(ctx, next){
            const course = await model.courseById(pool, ctx.params.id)
            //console.log(course)
            await ctx.render('table', {data: [course]})
            await next()
        }
    }
}