module.exports = function (model, pool) {
    return {
        async instructorsAll(ctx, next) {
            const instructors = await model.instructorsAll(pool)
            //console.log(instructors)
            await ctx.render('table', {data: instructors})
            await next()
        },
        async instructorsById(ctx, next){
            const instructor = await model.instructorById(pool, ctx.params.id)
            await ctx.render('table', {data: [instructor]})
            await next()
        }
    }
}