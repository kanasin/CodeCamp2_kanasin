class instructor {
    constructor(pool, row) {
        this._pool = pool;
         
        this.id = row.id,
        this.name = row.name,
        this.created_at = row.created_at
    }
    
}



module.exports = {
    async instructorsAll(pool){
        const [rows] = await pool.query(`select * from instructors`)
        return rows.map(row => new instructor(pool, row))
    },
    async instructorById(pool, id){
        const [rows] = await pool.query(`select * from instructors where id = ?`, [id])
        return new instructor(pool, rows[0])
    }
}