class course {
    constructor(pool, row) {
        this._pool = pool;
         
        this.id = row.id,
        this.name = row.name,
        this.detail = row.detail,
        this.price = row.price,
        this.teach_by = row.teach_by,
        this.created_at = row.created_at
    }
    
}




module.exports = {
    async courseByPrice(pool, price){
        const [rows] = await pool.query(`select * from courses where price = ?`, [price])
        return rows.map(row => new course(pool, row))
    },
    async courseById(pool, id){
        const [row] = await pool.query(`select * from courses where id = ?`, [id])
        return new course(pool, row[0])
    }
}