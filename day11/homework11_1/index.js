const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const path = require('path')
const fs = require('fs')
const util = require('util')

const app = new Koa()
const router = new Router()
const readFileAsyn = util.promisify(fs.readFile)
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'codecamp'
});

router.get('/',async ctx => {
    let temp = await myQuery()
    await ctx.render('homework11_1',{people: temp})
    console.log(temp)
})

router.get('/from_database',async (ctx, next) => {
    ctx.data = await myQuery()
    await next();
    ctx.body = ctx.data
})

router.get('/from_file',async (ctx, next) => {
    let data = await readFileAsyn('json/homework2_1.json', 'utf8')
    ctx.data = JSON.parse(data)
    await next();
    ctx.body = ctx.data
})



async function addVar(ctx) {
    let nowDate = new Date()
    let resultDate = nowDate.getFullYear()+"-"+(nowDate.getMonth()+1)+"-"+nowDate.getDate()+" "+nowDate.getHours()+":"+nowDate.getMinutes()+":"+nowDate.getSeconds()
    ctx.data = {data: ctx.data, additionalData: {userId: 1, dateTime: resultDate}}
    //ctx.body = {data: data, additionalData: {userId: 1, dateTime: 'test'}}
}
 

async function myQuery() {
    let [rows] = await pool.query('SELECT * FROM user');
    return rows
    //console.log('The solution is: ', rows);
}







app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.use(addVar)

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


app.listen(3000)