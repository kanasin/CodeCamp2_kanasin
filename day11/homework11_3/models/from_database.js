const db = require('./../lib/db.js')

const pool = db.pool

module.exports = {
    myQuery: async function(){
        let [rows] = await pool.query('SELECT * FROM user')
        return rows
    }
}

