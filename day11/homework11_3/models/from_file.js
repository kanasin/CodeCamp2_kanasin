const fs = require('fs')
const util = require('util')


module.exports = {
    readFileAsyn: util.promisify(fs.readFile)
}
