const mysql = require('mysql2/promise')


module.exports.pool = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'codecamp'
})


 