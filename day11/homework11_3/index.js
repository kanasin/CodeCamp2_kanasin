const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const db = require('./lib/db.js')
const queryFunc = require('./models/from_database.js')
const read = require('./models/from_file.js')


const app = new Koa()
const router = new Router()


router.get('/',async ctx => {
    let temp = await queryFunc.myQuery()
    await ctx.render('homework11_1',{people: temp})
    //console.log(temp)
})

router.get('/from_database',async (ctx, next) => {
    ctx.data = await queryFunc.myQuery()
    await next();
    ctx.body = ctx.data
})

router.get('/from_file',async (ctx, next) => {
    let data = await read.readFileAsyn('json/homework2_1.json', 'utf8')
    ctx.data = JSON.parse(data)
    await next();
    ctx.body = ctx.data
})



async function addVar(ctx) {
    let nowDate = new Date()
    let resultDate = nowDate.getFullYear()+"-"+(nowDate.getMonth()+1)+"-"+nowDate.getDate()+" "+nowDate.getHours()+":"+nowDate.getMinutes()+":"+nowDate.getSeconds()
    ctx.data = {data: ctx.data, additionalData: {userId: 1, dateTime: resultDate}}
    //ctx.body = {data: data, additionalData: {userId: 1, dateTime: 'test'}}
}
 









app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.use(addVar)

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


app.listen(3000)