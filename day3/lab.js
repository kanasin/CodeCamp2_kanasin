fetch('https://jsonplaceholder.typicode.com/posts/1').then(response => {
    return response.json();
 })
 .then(myJson => {
    console.log(myJson.userId);
 })
 .catch(error => {
    console.error('Error:', error);
 });
 