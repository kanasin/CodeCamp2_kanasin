fetch('../homework2_1.json').then(response => {
    return response.json();
})
    .then(employees => {
        let header;
        let tbody = '';
        let salary;
        for (const key in employees) {
            addAdditionalFields(employees[key])
            tbody = "<tr>";
            for (const key2 in employees[key]) {
                if (key2 === 'nextSalary') {
                    salary = "<ol>";
                    for (key3 in employees[key][key2]) {
                        salary += "<li>" + employees[key][key2][key3] + "</li>";
                    }
                    salary += "</ol>";
                    tbody += "<td>" + salary + "</td>";
                    salary = "";
                } else {
                    tbody += "<td>" + employees[key][key2] + "</td>";
                }
            }
            tbody += "</tr>";
            $('#table_json').append(tbody);
        }
        

    })
    .catch(error => {
        console.error('Error:', error);
    });




function addNextSalary(data) {
    let salary = parseInt(data.salary);
    data['nextSalary'] = [];
    for (let i = 0; i <= 2; i++) {
        if(i===0){
            data['nextSalary'][i] = salary;
            salary = data['nextSalary'][i];
        }else{
            data['nextSalary'][i] = ((salary * 10) / 100) + salary;
            salary = data['nextSalary'][i];
        }
    }
}


function addYearSalary(data) {
    data['yearSalary'] = parseInt(data.salary) * 12;
}



function addAdditionalFields(employees) {
    addYearSalary(employees);
    addNextSalary(employees);
}

