fetch('../homework2_1.json').then(response => {
    return response.json();
})
    .then(employees => {
        let header;
        let tbody = '';
        let salary;
        let newEmployees = [];
        for (const key in employees) {
            newEmployees[key] = addAdditionalFields(employees[key])
            tbody = "<tr>";
            for (const key2 in employees[key]) {
                if (key2 === 'nextSalary') {
                    salary = "<ol>";
                    for (key3 in employees[key][key2]) {
                        salary += "<li>" + employees[key][key2][key3] + "</li>";
                    }
                    salary += "</ol>";
                    tbody += "<td>" + salary + "</td>";
                    salary = "";
                } else {
                    tbody += "<td>" + employees[key][key2] + "</td>";
                }
            }
            tbody += "</tr>";
            $('#table_json').append(tbody);
        }
        newEmployees[1].salary = 100;
        newEmployees[0].nextSalary[0] = 50;
        console.log(newEmployees);
        console.log(employees[0].salary);
        console.log(newEmployees[0].salary);
        for (const key in newEmployees) {
            tbody = "<tr>";
            for (const key2 in newEmployees[key]) {
                if (key2 === 'nextSalary') {
                    salary = "<ol>";
                    for (key3 in newEmployees[key][key2]) {
                        salary += "<li>" + newEmployees[key][key2][key3] + "</li>";
                    }
                    salary += "</ol>";
                    tbody += "<td>" + salary + "</td>";
                    salary = "";
                } else {
                    tbody += "<td>" + newEmployees[key][key2] + "</td>";
                }
            }
            tbody += "</tr>";
            $('#table_json2').append(tbody);
        }
        

    })
    .catch(error => {
        console.error('Error:', error);
    });




function addNextSalary(data) {
    let salary = parseInt(data.salary);
    data['nextSalary'] = [];
    for (let i = 0; i <= 2; i++) {
        if(i===0){
            data['nextSalary'][i] = salary;
            salary = data['nextSalary'][i];
        }else{
            data['nextSalary'][i] = ((salary * 10) / 100) + salary;
            salary = data['nextSalary'][i];
        }
    }
    newdata = data;
    return newdata;
}


function addYearSalary(data) {
    let newdata = {};
    data['yearSalary'] = parseInt(data.salary) * 10;
    newdata = data;
    return newdata;
}



function addAdditionalFields(employees) {
    let newdata = {};
    addYearSalary(employees);
    addNextSalary(employees);
    for (const key in employees) {
        newdata[key] = employees[key];
    }
    return newdata;
}

