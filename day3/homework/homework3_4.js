'use strict'

const persons = [
    {
        "id": { 'a': 555, 'b': 666, c: { v: { s: 5 } } },
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": [40000, 44000, 48400]
    },
    {
        "id": "1001",
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": [40000, 44000, 48400]
    },

    {
        "id": "1002",
        "firstname": "Tony",
        "lastname": "Stark",
        "company": "Marvel",
        "salary": [1000000, 1100000, 1210000]
    },

    {
        "id": "1003",
        "firstname": "Somchai",
        "lastname": "Jaidee",
        "company": "Love2work",
        "salary": [20000, 22000, 24200]
    },

    {
        "id": "1004",
        "firstname": "Monkey D",
        "lastname": "Luffee",
        "company": "One Piece",
        "salary": [900000, 990000, 1089000]
    }
];
let datatest = [1, 2, 3];
let result = clone_obj(persons);

result[0].id.c.v.s = 22;
// console.log(result);
// console.log(persons[0].salary[0]);

result[0].firstname = 'kana';

console.log(persons);
console.log(result);



// result[1] = 5;
// console.log(datatest[1])


function clone_obj(oldObj) {
    let newObj;
    if (Array.isArray(oldObj)) {
        newObj = [];
    } else if (typeof (oldObj) === 'object') {
        newObj = {};
    } else {
        newObj = oldObj;
    }

    for (const key in oldObj) {
        if (Array.isArray(oldObj[key])) {
            newObj[key] = [];
            newObj[key] = clone_obj(oldObj[key]);
        } else if (typeof (oldObj[key]) === 'object') {
            newObj[key] = {};
            newObj[key] = clone_obj(oldObj[key]);
        } else {
            if (typeof (oldObj) === 'object') {
                newObj[key] = oldObj[key];
            } else {
                newObj = oldObj;
            }
        }
    }
    return newObj;
}



// function clone_obj(oldObj) {
//     let newObj;
//     if(Array.isArray(oldObj)){
//         newObj = [];
//     }else if(typeof(oldObj)==='object'){
//         newObj = {};
//     }

//         for (const key in oldObj) {
//             if(Array.isArray(oldObj[key])){
//                 newObj[key] = [];
//                 for (const key2 in oldObj[key]) {
//                     if(Array.isArray(oldObj[key][key2])){
//                         newObj[key][key2] = [];
//                         for (const key3 in oldObj[key][key2]) {
//                             newObj[key][key2][key3] = clone_obj(oldObj[key][key2]);
//                         }
//                     }else{
//                         newObj[key][key2] = clone_obj(oldObj[key][key2]);
//                     }
//                 }
//             }else if(typeof(oldObj[key])==='object'){
//                 newObj[key] = {};
//                 for (const key2 in oldObj[key]) {
//                     if(Array.isArray(oldObj[key][key2])){
//                         newObj[key][key2] = [];
//                         for (const key3 in oldObj[key][key2]) {
//                             newObj[key][key2][key3] = clone_obj(oldObj[key][key2]);
//                         }
//                     }else{
//                         console.log(key+" "+key2+" "+oldObj[key][key2]);
//                         newObj[key][key2] = clone_obj(oldObj[key][key2]);
//                     }
//                 }
//             }else{
//                 if(typeof(oldObj)==='object'){
//                     newObj[key] = oldObj[key];
//                 }else{
//                     newObj = oldObj;
//                 }

//             }
//         }
//     return newObj;
// }




// function getarray(array) {

// }

// function getobj(obj) {

// }