const Koa = require('koa')
const fs = require('fs')
const util = require('util')

const app = new Koa()
const readFileAsync = util.promisify(fs.readFile)

class Employee {
    constructor(firstname, lastname, salary) {
        this.firstname = firstname
        this.lastname = lastname
        this.tshirt = 'tshirt'

        this._salary = salary // simulate private variable

    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        
    }
    getSalary () {  // simulate public method
        return this._salary
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary)
        this.dressCode = 'suit'
    }
    async getSalary(){  // simulate public method
        return super.getSalary()*2;
    }
    async work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    async increaseSalary(employee, newSalary) {
        if(employee._salary>newSalary){
            console.log(`${employee.firstname}'s salary is less than before!!`)
        }else{
            console.log(`${employee.firstname}'s salary has been set to ${newSalary}`)
        }
    }
    async _golf () { // simulate private method
        this.dressCode = 'golf_dress'
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode)
        
    }
    async gossip(employee, text){
        console.log(`Hey ${employee.firstname}, ${text}`)
    }
    async _fire(employee){
        console.log(`${employee.firstname} has been fired! Dress with :${employee.tshirt}`)
    }
    async _hire(employee){
        console.log(`${employee.firstname} has been hired back! Dress with :${employee.tshirt}`)
    }
    async _seminar(){
        console.log(`He is going to seminar Dress with :${this.dressCode}`)
    }
    async employeesRaw(){
        let employee = await readFileAsync('homework2_1.json', 'utf8')
        this.employeesRaw = JSON.parse(employee)
        this.employees = this.employeesRaw.map( val => new Programmer(val.firstname, val.lastname, val.salary, val.id, 'front end'))
        console.log(this.employees)
    }
}

class Programmer extends Employee{
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary)
        this.id = id
        this.type = type
    }
    webDev(){
        console.log(`เขียนเว็บไซต์บริษัท`)
    }
    fixCom(){
        console.log(`ซ่อมคอม`)
    }
    windownInstall(){
        console.log(`ลง Windows`)
    }
}



let dev = new Programmer('firstname', 'lastname', 30000, 1001, 'frontend')
let somchai = new CEO("Somchai","Sudlor",30000);
let somsri = new Employee("Somsri","Sudsuay",22000);
somchai.gossip(somsri,"Today is very cold!");
somchai.work(somsri);

somchai.increaseSalary(somsri, 20);
somchai.increaseSalary(somsri, 25000);

somchai.employeesRaw()






app.listen(3000)