function rootReducer(state = { ...state }, action) {
    switch (action.type) {
      case 'getAuth':
        return { ...state }
      default:
        return state
    }
  }
  
  export default rootReducer