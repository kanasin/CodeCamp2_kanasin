import React, { Component } from 'react'
import Navbar from './Navbar'
import Main from './Main'


class App extends Component {
  state = {
    isAuth: false,
    email: '',
    password: ''
  }

  typeText = (e) => {
    this.setState(
      { [e.target.name]: e.target.value }
    )
  }

  signIn = async (e) => {
    e.preventDefault()
    const response = await fetch('http://localhost:8000/api/v1/auth/signin', {
      method: 'post',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      }),
      credentials: 'include'
    })
    const data = await response.json()
    if(response.status===200){
      console.log(this.state.isAuth)
      this.setState(
        {isAuth: true}
      )
      console.log(response)
    }else{
      console.log('not success')
    }
  }

  render() {
    return (
      <div className="App">
          <Navbar />
          <Main signIn={this.signIn} typeText={this.typeText} email={this.email} password={this.password} />
      </div>
    );
  }
}

export default App
