import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import SignUp from './pages/SignUp'
import SignIn from './pages/SignIn'
import Pikka from './pages/Pikka'
import CreatePikka from './pages/CreatePikka'


const Main = (props) => (
    <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/SignUp' component={SignUp} />
        <Route path='/SignIn' render={() => <SignIn signIn={props.signIn} typeText={props.typeText} email={props.email} password={props.password} />} />
        <Route path='/pikka/:id' component={Pikka} />
        <Route path='/createpikka' component={CreatePikka} />
    </Switch>
)


export default Main