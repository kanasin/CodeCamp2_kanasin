import React, { Component } from 'react'


const PictureCard = (prop) => {

        return(
            <div style={{width: '20%', margin: '100px 20px'}}>
                <div style={{height: '300px'}}>
                <a href={`/pikka/${prop.id}`}>
                    <img style={{width: '100%', height: '100%'}} src={prop.picture} />
                </a>
                </div>
                <div>{prop.caption}</div>
            </div>
        )

}


export default PictureCard