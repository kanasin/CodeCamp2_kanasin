import React, { Component } from 'react'
import PictureCard from './../PictureCard'


class Home extends Component {
    state = {
        pic: [
            {
                caption: '',
                commentCount: '',
                createdAt: '',
                id: '',
                likeCount: '',
                picture: ''
            }
        ],
        isLoading: true
    }

    async componentDidMount() {
        try {
            const response = await fetch('http://localhost:8000/api/v1/pikka')
            if (response.status === 200) {
                const data = await response.json()
                this.setState({
                    pic: [ ...data.list ]
                }, () => console.log(this.state.pic))
            }
        }finally{
            this.setState({isLoading: false})
        }
        
    }



    render() {
        const html = this.state.pic.map(val => <PictureCard key={val.id} caption={val.caption} picture={val.picture} id={val.id} />)
        if(this.state.isLoading==true){
            return  'Loading'
        }else{
            return (
                <div style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap' }}>
                    {html}
                </div>
            )
        }
    }
}


export default Home