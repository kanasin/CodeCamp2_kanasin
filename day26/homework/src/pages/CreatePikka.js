import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './../actions'

const mapStateToProps = state => {
 return {
   isAuth: state.isAuth
 }
}


class CreatePikka extends Component {
    uploadPic = async (e) => {
        e.preventDefault()
        const formData = new FormData()
        const caption = document.getElementById('caption').value
        const pictureFile = document.getElementById('picture').files[0]
        formData.append('caption', caption)
        formData.append('picture', pictureFile)
        try {
            const response = await fetch('http://localhost:8000/api/v1/pikka', {
                method: 'post',
                body: formData,
                credentials: 'include'
            })
            const data = await response.json()
            if(response.statusText==='OK'){
                this.props.history.push('/')
            }
            console.log(response)
        }finally{
            
        }
    }
    
    
    render(){
        console.log(mapStateToProps)
        return(
            <div>
                <form onSubmit={this.uploadPic}>
                    <input type='file' name='picture' id='picture' />
                    <input type='text' name='caption' id='caption' />
                    <button type='submit'>Upload</button>
                </form>
            </div>
        )
    }
}

export default connect(mapStateToProps, actions)(CreatePikka)

//export default CreatePikka