import React, { Component } from 'react'


class SignUp extends Component {
    state = {
        email: '',
        password: '',
        confirmPassword: ''
    }

    typeText = (e) => {
        this.setState(
            {[e.target.name]: e.target.value}
        )
    }

    register = async (e) => {
        e.preventDefault()
        if(this.state.password !== this.state.confirmPassword){
            alert('confirm password is not correct')
        }else{
            const response = await fetch('http://localhost:8000/api/v1/auth/signup', {
                method: 'post',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password
                })
            })
            const data = await response.json()
            console.log(response)
        }
        
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.register}>
                    <div style={{ textAlign: 'center', padding: '30px' }}>
                        <label style={{ textAlign: 'left' }}>Email: </label>
                        <br />
                        <input name="email" className="inputtext" type="email" value={this.state.email} onChange={this.typeText} />
                    </div>
                    <div style={{ textAlign: 'center', padding: '30px' }}>
                        <label style={{ textAlign: 'left' }}>Password: </label>
                        <br />
                        <input name="password" className="inputtext" type="password" value={this.state.password} onChange={this.typeText} />
                    </div>
                    <div style={{ textAlign: 'center', padding: '30px' }}>
                        <label style={{ textAlign: 'left' }}>Confirm Password: </label>
                        <br />
                        <input name="confirmPassword" className="inputtext" type="password" value={this.state.confirmPassword} onChange={this.typeText} />
                    </div>
                    <div style={{ textAlign: 'center', margin: '0px auto' }}>
                        <button type="submit" className="submitBtn">Send</button>
                    </div>

                </form>
            </div>
        )
    }
}


export default SignUp