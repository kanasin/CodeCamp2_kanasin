import React, { Component } from 'react'


class SignIn extends Component {
    render() {
        return (
            <div className="container">
                <form onSubmit={this.props.signIn}>
                    <div style={{ textAlign: 'center', padding: '30px' }}>
                        <label style={{ textAlign: 'left' }}>Email: </label>
                        <br />
                        <input name="email" className="inputtext" type="email" value={this.props.email} onChange={this.props.typeText} />
                    </div>
                    <div style={{ textAlign: 'center', padding: '30px' }}>
                        <label style={{ textAlign: 'left' }}>Password: </label>
                        <br />
                        <input name="password" className="inputtext" type="password" value={this.props.password} onChange={this.props.typeText} />
                    </div>
                    <div style={{ textAlign: 'center', margin: '0px auto' }}>
                        <button type="submit" className="submitBtn">Log In</button>
                    </div>

                </form>
            </div>
        )
    }
}


export default SignIn