import React, { Component } from 'react'
import PictureCard from './../PictureCard'

class Pikka extends Component {

    state = {
        pic: {},
        isLoading: true
    }
   
    async componentDidMount() {
        try {
            const response = await fetch(`http://localhost:8000/api/v1/pikka/${this.props.match.params.id}`)
            if (response.status === 200) {
                const data = await response.json()
                this.setState({
                    pic: { ...data }
                }, () => console.log(this.state.pic))
            }
        }finally{
            this.setState({isLoading: false})
        }
    }

    


    render(){
        if(this.state.isLoading==true){
            return  'Loading'
        }else{
            return(
                <div style={{width: '50%', textAlign: 'center', margin: '0px auto'}}>
                    <img style={{width: '100%', height: '100%', marginTop: '50px'}} src={this.state.pic.picture} />
                    <div>{this.state.pic.caption}</div>
                    <input type='text' name='comment' />
                    <button>comment</button>
                </div>
            )
        }
    }
}


export default Pikka