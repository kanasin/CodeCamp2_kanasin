const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'koa_login'
})
const user = require('../controller/user')
const userController = user(null, pool)
const assert = require('assert')



describe('test path /register_completed', function () {
    describe('#testRegis()', function () {
        it('error message should empty',async function () {
            ctx = {
                request: {
                    body: {
                        username: 'test',
                        password: 'asdfgh',
                        email: 'aaa.com'
                    }
                },
                render: (rout, data) => { }
            }
            next = () => { return true }
            const rs = await userController.testRegis(ctx, next)
            assert.deepEqual(rs.errorMessage, '')
        })
    })
})

// async function runTest() {
    
// }

// runTest()