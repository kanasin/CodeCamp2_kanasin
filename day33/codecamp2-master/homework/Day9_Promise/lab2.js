'use strict';

const fs = require('fs');

function writeDemo1() {
  return new Promise(function(resolve, reject) {
    fs.writeFile('demofile1.txt', 'test', 'utf8', function(err) {
      if (err)
        reject(err);
      else
        resolve();
    });
  });
}

function readDemo1() {
  return new Promise(function(resolve, reject) {
    fs.readFile('demofile1.txt', 'utf8', function(err, dataDemo1) {
      if (err)
        reject(err);
      else
        resolve(dataDemo1);
    });
  });
}

function writeDemo2(dataDemo1) {
  return new Promise(function(resolve, reject) {
    fs.writeFile('demofile2.txt', dataDemo1, 'utf8', function(err) {
      if (err)
        reject(err);
      else
        resolve("Promise Success!");
    });
  });
}


// async function aaa() {
//   try {
//     await writeDemo1();
//     let dataDemo1 = '123456';
//     await writeDemo2(dataDemo1);
//     console.log(dataDemo1);
//   } catch (error) {
//     console.error(error);
//   }
// }

// aaa();

async function copyFile() {
  try {
    let b = await writeDemo1();
    let dataDemo1 = await readDemo1();
    //let c = await writeDemo2(dataDemo1);
    console.log(await writeDemo2(dataDemo1));
  } catch (error) {
    console.error(error);
  }
}
//copyFile();

class test {
  constructor(myVar) {
    this.myNumber = "1234";
    this.myVar = myVar;
  }
  myMethod() {
    console.log(this.myNumber);
    return this;
  }
  myMethod2() {
    console.log(this.myVar);
    return this;
  }
}
let myTest = new test(567);
myTest.myMethod().myMethod2().myMethod().myMethod();
const myArr = [1,2,3];
//const myArr2 = myArr.map((x) => x+1 );
//console.log(myArr.map(function(x){ return x+1 }).map((x)=> x+1));
console.log(myArr.map((x)=> x+1 ).map((x)=> x+1));
//console.log(myArr.map((x) => x+1));

//const myArr = [1,2,3];
// const myArr3 = myArr.map(function(x){ return x+1 });
// const myArr4 = myArr3.map((x) => x+1);
// console.log(myArr4)

// function wait1() {
//   return new Promise( (resolve, reject) => {
//     setTimeout( () => {
//       resolve('wait1');
//     }, 3000);
//   });
// }
// function wait2() {
//   return new Promise( (resolve, reject) => {
//     setTimeout( () => {
//       resolve('wait2');
//     }, 3000);
//   });
// }

// const util = require('util');

// const readFileAsync = util.promisify(fs.readFile);
// function wait99(temp, callback) {
//  setTimeout(function(){
//    callback(null, temp + " : wait99");
//  },1000);
// }

// const wait99Async = util.promisify(wait99);
// async function testRead(filename) {
//   let temp2;
//   try {
//     const temp = await readFileAsync(filename, 'utf8');
//     temp2 = await wait99Async(temp);
//     console.log(temp2); // file content1 : wait99
//   } catch (exception) {
//     console.error(exception);
//   }
//  }
//  testRead('demofile1.txt');
 


/*
writeDemo1().then(function(){
  return readDemo1();
}).then(function(dataDemo1){
  return writeDemo2(dataDemo1);
}).then(function(data){
  console.log(data);
}).catch(function(error){
  console.error(error)
});
*/




// promisify
/*const util = require('util');

const readFileAsync = util.promisify(fs.readFile);
function wait99(temp, callback) {
  setTimeout(function(){
    callback(null,temp + "\n wait99");
  },1000);
}

const wait99Async = util.promisify(wait99);
async function testRead(filename) {
  let temp2;
  try {
    const temp = await readFileAsync(filename, 'utf8');
    temp2 = await wait99Async(temp);
    console.log(temp2); // file content1 : wait99
  } catch (exception) {
    console.error(exception);
  }
}
testRead('demofile1.txt');

const bluebird = require("bluebird");
bluebird.promisifyAll(fs.prototype);
console.log(fs.readFileAsync);
*/
/*function writeDemo1() {
    return new Promise(function(resolve, reject) {
      fs.writeFile('demofile1.txt', 'test', 'utf8', function(err) {
        if (err)
          reject(err);
        else
          resolve('1','2');
      });
    });
}
function readDemo1(param1, param2) {
    return new Promise(function(resolve, reject) {
        fs.readFile('demofile1.txt', 'utf8', function(err, dataDemo1) {
        if (err)
          reject(err);
        else
          resolve(param1);
      });
    });
}

writeDemo1(function(param1, param2) {
  return readDemo1(param1, param2)
}).then(function(a,b){
  console.log(a,b);
})*/