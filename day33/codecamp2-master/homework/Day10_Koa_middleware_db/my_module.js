module.exports = function (myVar) {
    return {
        func1(){
            console.log(myVar+': func1');
        },
        func2(){
            console.log(myVar+': func2');
        }
    }
}