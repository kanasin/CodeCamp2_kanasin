module.exports = {
    async usernameExisted(pool, username) {
        const [usernameQuery] = await pool.query(`
            SELECT username FROM user
            WHERE username = ?
        `, [username]);
        console.log(usernameQuery);
        if (usernameQuery[0] && usernameQuery[0].username) {
            return true;
        } else {
            return false;
        }
    },
    async emailExisted(pool, email) {
        const [emailQuery] = await pool.query(`
            SELECT email FROM user
            WHERE email = ?
        `, [email]);
        
        if (emailQuery[0] && emailQuery[0].email) {
            return true;
        } else {
            return false;
        }
    },
    async insertUser(pool, username, password, email) {
        const aData = [
            username, 
            this.stupidHash(password),
            email
        ];
        const sql = `INSERT INTO user (username, password, email) VALUES (?, ? ,?)`;
        const [resultInsert] = await pool.query(sql, aData);
    
        return resultInsert.insertId;
    },
    async getUserInfo(pool, userId) {
        const [results] = await pool.query(`
                SELECT id, email 
                FROM user 
                WHERE id = ?
            `, [userId]);

        return {
            userId: results[0].userId,
            email: results[0].email
        };
    },
    stupidHash(password) {
        return '123' + password + '123';
    }
}