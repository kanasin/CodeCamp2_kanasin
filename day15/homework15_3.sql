create database minimart CHARACTER SET UTF8 COLLATE utf8_unicode_ci;

use minimart;

create table employee (id int auto_increment, name varchar(100), address varchar(250), salary int, department_id int, primary key(id));
create table department (id int auto_increment, name varchar(100), budget int, primary key(id));
create table customer (id int auto_increment, name varchar(100), address varchar(250), primary key(id));
create table order_bill (id int auto_increment, customer_id int, employee_id int, order_date timestamp default now(), primary key(id));
create table order_item (id int auto_increment, order_bill_id int, amount int, discount int,product_id int, primary key(id));
create table product (id int auto_increment, name varchar(200), description text,supplier_id int, price int, quantity int, primary key(id));
create table supplier (id int auto_increment, name varchar(200), address varchar(250), phone_no varchar(10), primary key(id));