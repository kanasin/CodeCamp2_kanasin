const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const util = require('util')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'codecamp'
});
const execute = util.promisify(pool.execute)


router.get('/', async ctx => {
    let sqlNoTeach = "select courses.name from courses left join instructors on courses.teach_by = instructors.id where instructors.name is null"
    let sqlNocourse = "select instructors.name from courses right join instructors on courses.teach_by = instructors.id where courses.name is null"
    let data = {}
    data.courseName = await select_data(sqlNoTeach)
    data.teacherName = await select_data(sqlNocourse)
    await ctx.render('table', data)
})



router.get('/course', async ctx => {
    let courseName = ctx.query.course_name
    let whereClaus = ''
    if(courseName){
        whereClaus = ` and name like '%${courseName}%'`
    }
    let sqlNoTeach = `select * from courses where 1=1 ${whereClaus}`
    let data = {}
    data.courseNameSearch = courseName
    data.course = await select_data(sqlNoTeach)
    await ctx.render('course', data)
})

router.get('/course/:course_name', async ctx => {
    data = {}
    const [rows] = await pool.query(`select courses.*, instructors.name as teacher from courses inner join instructors on instructors.id = courses.teach_by where courses.name = ?`,[ctx.params.course_name])
    data.course_name = rows[0].name
    data.price = rows[0].price
    data.teacher = rows[0].teacher
    const [student] = await 
    console.log(rows)
})



async function select_data(query) {
    let [rows] = await pool.query(query);
    return rows
    //console.log('The solution is: ', rows);
}


app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})





app.listen(3000)