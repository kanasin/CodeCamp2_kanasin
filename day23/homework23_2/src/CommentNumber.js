import React, { Component } from 'react'

class CommentNumber extends Component {
  render() {
    return (
        <div style={{margin: '5px'}}>
            <button type="button" className="btn btn-default btn-sm">
            <i className="far fa-comment"></i> comment 0
            </button>
        </div>
    )
  }
}

export default CommentNumber
