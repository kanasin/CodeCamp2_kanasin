import React, { Component } from 'react'
import LikeBtn from './LikeBtn'
import CommentNumber from './CommentNumber'


const pic = ['pikkanode.png', 'pikkanode.png', 'pikkanode.png', 'pikkanode.png', 'pikkanode.png']
class PictureBox extends Component {
    render() {
        const box = pic.map( val => 
            <div className="col-lg-3" style={{padding: '25px'}}>
                <div style={{marginBottom: '15px'}}>
                    <img src={val} style={{width: '100%'}} />
                </div>
                <div>
                    Sat Jul 07 2018 15:06:49
                </div>
                <div className="form-inline">
                    <LikeBtn />
                    <CommentNumber />
                </div>
            </div>
        )
        return <div className="row">{box}</div>
    }
}

export default PictureBox
