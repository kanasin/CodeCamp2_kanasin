import React, { Component } from 'react'
import PictureBox from './PictureBox'
import './App.css'
import Menu from './Menu'
import Footer from './Footer'

class App extends Component {
  render() {
    return (
      <div>
        <Menu />
        <div className="container">
          <PictureBox />
        </div>
          <Footer />
      </div>
    )
  }
}

export default App
