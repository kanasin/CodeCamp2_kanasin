import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import RandomBox from './RandomBox'
import World from './World'

ReactDOM.render(<World />, document.getElementById('root'))
