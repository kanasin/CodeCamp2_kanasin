import React, { Component } from 'react'


const color = ['red', 'blue', 'green', 'purple', 'pink']
const pickedColor = color[getRandomArbitrary(color.length)]
const sizeFont = getRandomArbitrary(40, 20)
console.log(sizeFont)

const RandomBox = () => {
    return  <div style={{ textAlign: 'center', height: '300px', width: '300px', backgroundColor: pickedColor }}>
            <span style={{fontSize: sizeFont, verticalAlign: 'middle'}}>
                Random Box
            </span>
            </div>
}

function getRandomArbitrary(max, min = 0) {
    return Math.floor(Math.random() * (max - min) + min)
  }


export default RandomBox