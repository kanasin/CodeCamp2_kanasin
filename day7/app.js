let people = ['k', 'n', 5, 6, 'hg']
let p2 = people.filter((val)=>{
    return !isNaN(val)
}).map((val)=>{
    return val*10
}).reduce((sum, val)=>{
    return sum+val
}, 0);

console.log(p2);