const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')
const util = require('util')

const readFileAsync = util.promisify(fs.readFile)

const app = new Koa()
const router = new Router()



router.get('/', async ctx => {
    const data = await readFileAsync('public/assets/homework2_1.json', 'utf8')
    const dataSend = {}
    dataSend.people = JSON.parse(data)
    await ctx.render('index', dataSend)
})






app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})





app.listen(3000)