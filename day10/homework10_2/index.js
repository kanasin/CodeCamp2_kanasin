const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()


router.get('/',async ctx => {
    data = {
        page: "index"
    }
    await ctx.render('index', data)
})

router.get('/contract',async ctx => {
    data = {
        page: "contrack"
    }
    await ctx.render('contrack', data)
})

router.get('/portfolio',async ctx => {
    data = {
        page: "portfolio"
    }
    await ctx.render('portfolio', data)
})

router.get('/regis',async ctx => {
    data = {
        page: "regis"
    }
    await ctx.render('register_form', data)
})

router.get('/skill',async ctx => {
    data = {
        page: "skill"
    }
    await ctx.render('skill', data)
})





app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})



app.listen(3000)