import React, { Component } from 'react'
import Item from './Item'

class App extends Component {
  
  _arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]

  state = {
    item: this._arr.map(val => val = {id: val, color: 'grey'})
  }

  changeColor = (e) => {
    let id = parseInt(e.target.id)
    this.setState(
      {item: this.state.item.map(val => (val.id===id || val.id===id+5 || val.id===id-5 || val.id===id+1 || val.id===id-1) ? val = { ...val, color: (val.color==='grey') ? '#9af2f5' : 'grey' } : val )}
    )
  }
  

  render() {
    let html = this.state.item.map(val => <Item key={val.id} idNumber={val.id} color={val.color} changeColor={this.changeColor} />)
    return (
      <div style={{display: 'flex', width: '260px',  flexWrap: 'wrap'}}>
        {html}
      </div>
    )
  }
}


export default App