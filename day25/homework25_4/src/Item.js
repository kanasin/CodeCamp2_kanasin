import React, { Component } from 'react'



class Item extends Component{
    render(){
        return(
            <div onClick={this.props.changeColor} id={this.props.idNumber} style={{backgroundColor: this.props.color, height: '50px', width: '50px', border: '1px solid #fff', borderRadius: '5px'}}></div>
        )
    }
}


export default Item