import React, { Component } from 'react'

class Item extends Component {
    
     
  render() {
    //console.log(this.props)
    return (
      <div key={this.props.index} name={`item${this.props.index}`}>
        {this.props.menu}
        <button style={{backgroundColor: 'red'}} value={this.props.index} onClick={this.props.delete} >ลบ</button>
        <input name={`textEdit${this.props.index}`} value={this.props.menu} onChange={this.props.edit} />
        <hr/>
      </div>
        
    )
  }
}



export default Item
