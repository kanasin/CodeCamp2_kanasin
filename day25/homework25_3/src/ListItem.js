import React, { Component } from 'react'
import PropTypes from "prop-types"
import Item from './Item'

class ListItem extends Component {
    static defaultProps = {
        todoList: [
            {
                text: `ยังไม่มีรายการ`
            }
        ]
    }
    static propType = {
        todoList: PropTypes.array
    }

    
    render() {
        let html = this.props.todoList.map((val, index) => <Item menu={val.text} index={index} delete={this.props.delete} edit={this.props.edit} />)
        return (
            <div>
                {html}
            </div>
    )
    }
}

export default ListItem
