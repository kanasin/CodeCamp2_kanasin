import React, { Component } from 'react'
import AddForm from './AddForm'
import ListItem from './ListItem'

class App extends Component {
  state = {
    inputText: '',
    todoList: []
  }

  editeText = (e) => {
    this.setState(
      {[e.target.name]: e.target.value}
    )
  }

  addTodo = () => {
    this.setState(
      {todoList: [ ...this.state.todoList, {text: this.state.inputText} ]}
    )
  }

  delete = (e) => {
    this.setState(
      {todoList: this.state.todoList.filter((val, key) => e.target.value!=key )}
    )
  }

  edit = (e) => {
    this.setState(
      {todoList: this.state.todoList.map((val, key) => e.target.name!==`textEdit${key}` ? val : {text: e.target.value} )}
    )
  }

  render() {
    return (
      <div>
        <AddForm text={this.state.inputText} editeText={this.editeText} addTodo={this.addTodo} />
        <ListItem todoList={this.state.todoList} delete={this.delete} edit={this.edit} />
      </div>
    )
  }
}

export default App
