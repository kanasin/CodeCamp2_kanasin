import React, { Component } from 'react'
import PropTypes from "prop-types"

class AddForm extends Component {
    
      static propType = {
        text: PropTypes.string,
        editeText: PropTypes.func,
        addTodo: PropTypes.func
      }
     
  render() {
    return (
        <div>
            <input type="text" name="inputText" value={this.props.text} onChange={this.props.editeText} />
            <button type="button" onClick={this.props.addTodo} >AddList</button>
        </div>
    )
  }
}



export default AddForm
