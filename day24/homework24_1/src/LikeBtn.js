import React, { Component } from 'react'

class LikeBtn extends Component {
  render() {
    return (
        <div>
            <button type="button" className="btn btn-default btn-sm">
            <i className="fab fa-angellist"></i> Like {this.props.count}
            </button>
        </div>
    )
  }
}

export default LikeBtn
