import React, { Component } from 'react'
import PictureBox from './PictureBox'
import './App.css'
import Menu from './Menu'
import Footer from './Footer'

class App extends Component {
  render() {
    
    let arrayMap = [1, 2, 3, 4, 5]
    let data = arrayMap.map( val => val = {
      id: Math.floor(Math.random()), 
      imgSrc: 'pikkanode.png', 
      createBy: 'PanotlnwZa', 
      date: new Date(),
      likeCount: Math.floor(Math.random() * Math.floor(99)),
      commentCount: Math.floor(Math.random() * Math.floor(99))
    })
   
    let createBox = data.map( val => <PictureBox
      key={val.id}
      pic={val.imgSrc}
      createBy={val.createBy}
      date={val.date}
      likeCount={val.likeCount}
      commentCount={val.commentCount}
    />)
    return (
      <div>
        <Menu />
        <div className="container">
          <div className="row">
            {createBox}
          </div>
        </div>
          <Footer />
      </div>
    )
  }
}



export default App
