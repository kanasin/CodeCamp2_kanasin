import React, { Component } from 'react'
import LikeBtn from './LikeBtn'
import CommentNumber from './CommentNumber'



class PictureBox extends Component {
    render() {
        
        return <div className="col-lg-3" style={{ padding: '25px' }} key={this.props.id}>
            <div style={{ marginBottom: '15px' }}>
                <img src={this.props.pic} style={{ width: '100%' }} />
            </div>
            <div>
                Created By: {this.props.createBy}
                Created Date: {this.props.date.toString()}
            </div>
            <div className="form-inline">
                <LikeBtn
                    count={this.props.likeCount}
                />
                <CommentNumber
                    count={this.props.commentCount}
                />
            </div>
        </div>
    }
}

export default PictureBox
