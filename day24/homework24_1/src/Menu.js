import React, { Component } from 'react'

const Menu = () => {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="#">
                <img src="pikkanode.png" style={{height: '70px'}} />
                </a>

                <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item">
                            <a className="nav-link" href="#">Create Pikka</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">SignUp</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">SignIn</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">SignOut</a>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }

export default Menu
