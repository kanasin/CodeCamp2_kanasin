import React, { Component } from 'react'
import Card from './Card'

class App extends Component {
  state = {
    card: []
  }
  
  color = ['red', 'blue', 'green', 'purple', 'pink']
  numberCard = [1,2,3,4,5,6,7,8]

  getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  }
  
  ShuffleCard = () => {
    this.setState(
      {card: this.state.card.map(val => val = {color: this.color[this.getRandomInt(4)]})}
    )
  }
  
  showColor = (e) => {
    alert(e.target.className)
  }


  render() {
    this.state.card = this.numberCard.map(val => val = {color: this.color[this.getRandomInt(4)]})
    let html = this.state.card.map(val => <Card color={val.color} showColor={this.showColor} />)
    return (
      <div style={{display: 'flex',justifyContent: 'center',margin: '0px auto',flexWrap: 'wrap'}}>
        {html}
        <button onClick={this.ShuffleCard}>Shuffle</button>
      </div>
    )
  }
}

export default App
