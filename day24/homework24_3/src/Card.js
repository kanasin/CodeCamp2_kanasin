import React, { Component } from 'react'


class Card extends Component {
    static defaultProps = {
        color: '#ececec'
    }

   

    render(){
        return(
            <div onClick={this.props.showColor} color={this.props.color} className={this.props.color} style={{width: '20%', height: '200px', backgroundColor: this.props.color, margin: '2%'}} ></div>
        )
    }
}


export default Card