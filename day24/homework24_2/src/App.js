import React, { Component } from 'react'
import './App.css'

class App extends Component {
  state = {
    pic: '',
    email: '',
    gender: '',
    name: ''
  }
  randomUser = async () => {
    let response = await fetch('https://randomuser.me/api/')
    const data = await response.json()
    this.setState({
      pic: data.results[0].picture.large,
      email: data.results[0].email,
      gender: data.results[0].gender,
      name: `${data.results[0].name.title} ${data.results[0].name.first} ${data.results[0].name.last}`
    })
    //console.log(data)
  }
  

  render() {
    return (
      <div>
        <img src={this.state.pic} />
        <p>Email: {this.state.email}</p>
        <p>gender: {this.state.gender}</p>
        <p>name: {this.state.name}</p>
        <br />
        <button onClick={this.randomUser}>Generate User</button>
      </div>
    )
  }
}

export default App;
