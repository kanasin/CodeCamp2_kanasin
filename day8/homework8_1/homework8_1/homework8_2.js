'use strict'
const fs = require('fs');
let file = ['head', 'body', 'leg', 'feet']
let i = 0
let result

const callbackread = (err, data) => {
    if (err) {
        console.error(err);
        return;
    }
    result = data+"\n"
    console.log(data);
    writefile()
}

const callbackwrite = (err) => {
    if (err) {
        console.error(err);
        return;
    }
    readall()
    i++
}

function readall(){
    if (i < file.length) {
        fs.readFile(`robot.txt`, 'utf8', callbackread)
    }
}

function writefile(){
    if (i < file.length) {
        fs.writeFile(`robot.txt`, `${result}I\'m ${file[i]}.`, 'utf8', callbackwrite)
    }
}


fs.writeFile(`robot.txt`, `I\'m ${file[i]}.`, 'utf8', callbackwrite)
