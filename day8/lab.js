'use strict'
const fs = require('fs');

let callbackValue;
function hello6(err, value) {
  callbackValue = value;
  console.log(callbackValue); // print callbackValueNew after 3 seconds
} 

callbackValue = 'callbackValueOld';
function tryHello6(callbackFunction) {
  let returnValue = 'returnValue';
  let callbackValue = 'callbackValueNew';
  setTimeout(callbackFunction, 3000, null, callbackValue);
  return returnValue;
}

console.log(tryHello6(hello6)); // print returnValue
console.log(callbackValue); // print callbackValueOld




// function addSync(a, b) {
//   return a + b;
// }
// let sum = addSync(1,2);
// function add(a, b, callbackFunction) {
//   callbackFunction( null, a + b );
// }
// let sum2;
// add(1, 2, (err, returnValue) => {
//   sum2 = returnValue;
// });

// console.log(sum2)