const arr = [3,44,38,5,47,15,36,27,2,46,4,19,50,48]

function swap(myArray, index, index2) {
    let temp;
    temp = myArray[index];
    myArray[index] = myArray[index2];
    myArray[index2] = temp;
}

 
function sortArr(mode, arr){
    let numIndex = arr.length - 1
    let randomNUm = Math.floor(Math.random() * numIndex)
    if(mode=='random'){
        arr.forEach((val, index) => {
            randomNUm = Math.floor(Math.random() * numIndex)
            swap(arr, index, randomNUm)
        })
    }else if(mode=='asc'){
        let tmpMin
        let minIndex
        arr.forEach((val, index) => {
            tmpMin = val
            arr.forEach((val2, index2) => {
                if(val2<tmpMin && index2>index){
                    tmpMin = val2
                    swap(arr, index, index2)
                }
            })
        })
    }else if(mode=='desc'){
        let tmpMax
        arr.forEach((val, index) => {
            tmpMax = val
            arr.forEach((val2, index2) => {
                if(val2>tmpMax && index2>index){
                    tmpMax = val2
                    swap(arr, index, index2)
                }
            })
        })
    }
}


sortArr('asc', arr)

console.log(arr)