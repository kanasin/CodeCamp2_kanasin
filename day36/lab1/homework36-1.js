const arr = [3,44,38,5,47,15,36,27,2,46,4,19,50,48]

function swap(myArray, index, index2) {
    let temp;
    temp = myArray[index];
    myArray[index] = myArray[index2];
    myArray[index2] = temp;
}


function bubbleSort(arr){
    let swaping = true
    while (swaping) {
        swaping = false
        for(let i=0;i<arr.length-1;i++){
            if(arr[i] > arr[i+1]){
                swap(arr, i, i+1)
                swaping = true
            }
        }
    }
}

bubbleSort(arr)

console.log(arr)