const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const serve = require('koa-static')

const app = new Koa()
const router = new Router()
const pool = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'codecamp'
})


router.get('/', async ctx => {
    let [course] = await pool.query('select courses.name, sum(courses.price) AS price from courses inner join enrolls on courses.id = enrolls.course_id group by courses.id')
    let [student] = await pool.query('SELECT students.name, SUM(courses.price) AS price from students INNER JOIN enrolls ON students.id = enrolls.student_id INNER JOIN courses ON courses.id = enrolls.course_id GROUP BY students.id')
    let data = {}
    data.course = course
    data.student = student
    console.log(data)
    await ctx.render('table_students', data)
})











async function select_data(query) {
    let [rows] = await pool.query(query);
    return rows
    //console.log('The solution is: ', rows);
}

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})



app.listen(3000)