const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const mysql = require('mysql2/promise')
const render = require('koa-ejs')
const fetch = require('isomorphic-unfetch')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'Book_Store'
});


router.get('/',async ctx => {
    let employeeSql = "select firstname, lastname, age from employee"
    let booksSql = "select isbn, book_name, price from books"
    let data = {}
    data.employee = await select_data(employeeSql)
    data.books = await select_data(booksSql)
    await ctx.render('index', data)
})

router.get('/add',async ctx => {
    fetch('https://randomuser.me/api')
    .then( r => r.json() )
    .then( data => {
        let result = []
        result = data.results
        result.forEach(val => {
            let dob = new Date(val.dob)
            let birth = dob.getFullYear()+"-"+dob.getMonth()+"-"+dob.getDate()
            let newName = val.name.first.charAt(0).toUpperCase() + val.name.first.slice(1)
            let newLastName = val.name.last.charAt(0).toUpperCase() + val.name.last.slice(1)
            pool.execute(
                "insert into employee(firstname, lastname, age) values ( ?,?,(YEAR(CURDATE())-YEAR(?)) )",
                [newName, newLastName, birth],
                function(err, results, fields) {
                    console.log(results); // results contains rows returned by server
                    console.log(fields); // fields contains extra meta data about results, if available
                }
            )
        });
    });
})





async function select_data(query) {
    let [rows] = await pool.query(query);
    return rows
    //console.log('The solution is: ', rows);
}


async function manage_data(query) {
    let [rows] = await pool.query(query);
    //console.log('The solution is: ', rows);
}


app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


app.listen(3000)