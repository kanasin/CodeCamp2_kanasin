const Koa = require('koa');
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const fs = require('fs')
const util = require('util')
const pug = require('pug')


const readFileAsync = util.promisify(fs.readFile)
const app = new Koa();
const router = new Router()


router.get('/',async ctx => {
    let page = {
        page: 'index'
    }
    let html = await pug.renderFile('public/views/index.pug', page)
    ctx.body = html
})

router.get('/skill',async ctx => {
    let page = {
        page: 'skill'
    }
    let html = await pug.renderFile('public/views/skill.pug', page)
    ctx.body = html
})

router.get('/contact',async ctx => {
    let page = {
        page: 'contact'
    }
    let html = await pug.renderFile('public/views/contact.pug', page)
    ctx.body = html
})

router.get('/portfolio',async ctx => {
    let page = {
        page: 'port'
    }
    let html = await pug.renderFile('public/views/portfolio.pug', page)
    ctx.body = html
})

router.get('/regis',async ctx => {
    let page = {
        page: 'regis'
    }
    let html = await pug.renderFile('public/views/regis.pug', page)
    ctx.body = html
})








const PORT = 3000

app.use(serve(path.join(__dirname, 'public/images')))
app.use(router.routes())
app.use(router.allowedMethods())




app.listen(PORT)
//console.log(`Service is running on port ${PORT}.`)