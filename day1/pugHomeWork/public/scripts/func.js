let day = 31;
let month = 12;
let year = 50;
for (let index = 1; index <= day; index++) {
    $('#day').append($("<option></option>")
        .attr("value", index)
        .text(index));

}
for (let index = 1; index <= month; index++) {
    $('#month').append($("<option></option>")
        .attr("value", index)
        .text(index));

}
for (let index = 1980; index <= 2005; index++) {
    $('#year').append($("<option></option>")
        .attr("value", index)
        .text(index));

}

function check_ep(e, ce, p, cp) {
    if (e !== ce) {
        alert('ยืนยันอีเมล์ไม่ถูกต้องกรุณากรอกใหม่');
        $('#confirm_email').focus();
        return;
    } if (p !== cp) {
        alert('ยืนยันพาสเวิร์ดไม่ถูกต้องกรุณากรอกใหม่');
        $('#confirm_password').focus();
        return;
    } else {
        $('#frm_submit').submit();
    }
}